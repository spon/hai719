// Définition de la classe Scene

// superclasses et classes nécessaires
Requires("Spoke");


class Scene
{
    /** constructeur */
    constructor()
    {
        // créer les objets à dessiner
        this.m_spoke = new Spoke();

        // couleur du fond : gris foncé
        gl.clearColor(0.4, 0.4, 0.4, 0.0);
    }


    /**
     * appelée quand la taille de la vue OpenGL change
     * @param width : largeur en nombre de pixels de la fenêtre
     * @param height : hauteur en nombre de pixels de la fenêtre
     */
    onSurfaceChanged(width, height)
    {   
        let y_offset = width/4;
        // met en place le viewport
        gl.viewport(0, 0, y_offset, height);
        gl.viewport(y_offset, 0, y_offset, height);
        // gl.viewport(400, 400, width, height);
    }


    /**
     * Dessine l'image courante
     */
    onDrawFrame(width, height)
    {

        // effacer l'écran
        gl.clear(gl.COLOR_BUFFER_BIT);

        let x_offset = width/4;
        let y_offset = height/2;
        // met en place le viewport
        gl.viewport(0, y_offset, x_offset, y_offset);
        this.m_spoke.onDraw_0();

        gl.viewport(x_offset, y_offset, x_offset, y_offset);
        this.m_spoke.onDraw_1();

        gl.viewport(2 * x_offset, y_offset, x_offset, y_offset);
        this.m_spoke.onDraw_2();

        gl.viewport( 3 * x_offset, y_offset, x_offset, y_offset);
        this.m_spoke.onDraw_3();

        gl.viewport( 0, 0, x_offset, y_offset);
        this.m_spoke.onDraw_4();

        gl.viewport( 2 * x_offset, 0, x_offset, y_offset);
        this.m_spoke.onDraw_5();

    }


/** supprime tous les objets de cette scène */
    destroy()
    {
        this.m_spoke.destroy();
    }
}
