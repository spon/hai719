# Exercice 1

2.

```c++
//Calcul de P3
Vec2 u = normalize((P2 - P1));	// u est le vecteur directeur de P1P2
Vec2 v = (P4 - P2);		// vecteur P2P4
    
// projection de v sur u						
P4P3 = (dot_product(u,v) / dot_product(u, u)) * u;

// en connaissant la position de P4 on peut alors déduire P3:
P3 = P4P3 + P4;
P3 = 0.2985584727570576,0.11002785258730308;

//Calcul de P7
Vec2 u = normalize((P6 - P5));	// u est le vecteur directeur de P5P6
Vec2 v = (P8 - P6);		// vecteur P6P8
    
// projection de v sur u						
P8P7 = (dot_product(u,v) / dot_product(u, u)) * u;

// en connaissant la position de P8 on peut alors déduire P7:
P7 = P8P7 + P8;
P7 = 0.38461598785015844,0.06608166454782374;

```



3. Pour afficher ce motif avec webgl et glsl il faut utiliser la **projection orthographique**, nous aurons aussi besoin des **transformations de la modélisation** ainsi que des **transformations de la caméra**.
4. Pour passer de l'étape **(a)** à l'étape **(b)** on a appliqué 5 fois une **rotation sur l'axe Z** avec pour origine de la rotation le point **P0**. De l'étape **(b)** à **(c)** il  a été appliqué une **symétrie par rapport à l'axe X**. Enfin, pour arriver à l'étape **(d)** on a appliqué la même **rotation sur l'axe Z** qu'en **(b)** avec le **symétrique** obtenu en **(c)**.
5. On a appliqué une mise à l'échelle avec une petite valeur (<1) sur le motif en **3(a)**. Pour la figure **3(b)** il semble que la caméra a été déplacée à droite et s'est rapprochée du motif. Cependant en appliquant une matrice orthogonale on arrive à avoir ce genre de résultat visuellement.

