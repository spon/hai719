
let dotProduct = function(u, v) {
    let res = 0;

    u.forEach((elem, ind) => {
        res += elem * v[ind];

    });

    return res;
}

let vectAdd = function(u, v) {
    let resVect = u;

    resVect.forEach((elem, ind) => {
        resVect[ind] += v[ind];
    });

    return resVect;
}

let vectMult = function(scalar, v) {
    let resVect = v.slice();

    resVect.forEach((elem, ind) => {
        resVect[ind] *= scalar;
    });
    return resVect;
}

let normalize = function(v) {
    return vectMult(1/Math.sqrt(dotProduct(v, v)), v);
}

let getP3orP8 = function(P1, P2, P4) {
    let u = [0, 0];

    //construction du vecteur directeur de la droite P1P2:(u)
    u.forEach((elem, index) => {
        u[index] += P2[index] - P1[index];
    });

    //faire la projection du vecteur P4P2 sur la droite P4 + u
    
    //construction du vecteur P4P2 (v)
    let v = [0, 0];
    
    v.forEach((elem, index) => {
        v[index] = P2[index] - P4[index];
    });
    

    u = normalize(u)
    
    //projection
    let P3 = [0, 0];

    //on doit projeter v sur u

    let prodScal = dotProduct(u, v);
    let P3P4 = vectMult(prodScal / dotProduct(u, u), u);  //vRes est sensé être le vecteur ayant pour origin

    return P3P4

}

class Spoke
{
    /** constructeur */
    constructor()
    {
        /** shader */

        let srcVertexShader = dedent
            `#version 300 es
            uniform mat4 rotationZmatrix;
            uniform mat4 symetricYmatrix;
            uniform mat4 scaleMatrix;
            in vec2 glVertex;
            void main()
            {
                gl_Position = scaleMatrix * symetricYmatrix * rotationZmatrix * vec4(glVertex, 0.0, 1.0);
            }`;

        let srcFragmentShader = dedent
            `#version 300 es
            precision mediump float;
            out vec4 glFragColor;
            void main()
            {
                glFragColor = vec4(0.9, 0.9, 0.9, 1.0);
            }`;

        // compiler le shader de dessin
        this.m_ShaderId = Utils.makeShaderProgram(srcVertexShader, srcFragmentShader, "Spoke");

        // déterminer où sont les variables attribute
        this.m_VertexLoc = gl.getAttribLocation(this.m_ShaderId, "glVertex");

        /** VBOs */

        // créer et remplir le buffer des coordonnées
        this.vertices = [
                0.0, 0.0109,
                0.2174, 0.0109,

                0.2174, 0.0109,
                0.2826, 0.1196,

                0.2826, 0.1196,
                0.2994, 0.1104,     // P3

                0.2994, 0.1104,     // P3
                0.2391, 0.0109,

                0.2391, 0.0109,
                0.3261, 0.0109,

                0.3261, 0.0109,
                0.3696, 0.0761,

                0.3696, 0.0761,
                0.386, 0.0667,

                0.386, 0.0667,
                0.3478, 0.0109,     //P8

                0.3478, 0.0109,
                0.4783, 0.0109,

                0.4783, 0.0109,
                0.5, 0, 0        
        ];
        this.m_VertexBufferId = Utils.makeFloatVBO(this.vertices, gl.ARRAY_BUFFER, gl.STATIC_DRAW);
    }


    onDraw_0()
    {   
    
        let P1 = [0.2174, 0.0109];
        let P2 = [0.2826, 0.1196];
        let P4 = [0.2391, 0.0109];

        let P5 = [0.3261, 0.0109];
        let P6 = [0.3696, 0.0761];
        let P8 = [0.3478, 0.0109];

        let P4P3 = getP3orP8(P1, P2, P4);
        let P8P7 = getP3orP8(P5, P6, P8);

        let P3 = vectAdd(P4P3, P4);
        let P7 = vectAdd(P8P7, P8);
        console.log(`computed P3= ${P3} == [0.2994, 0.1104]?`);
        console.log(`computed P7= ${P7} == [0.386, 0.0667]?`);



        // activer le shader
        gl.useProgram(this.m_ShaderId);

        // activer et lier le buffer contenant les coordonnées
        gl.bindBuffer(gl.ARRAY_BUFFER, this.m_VertexBufferId);
        gl.enableVertexAttribArray(this.m_VertexLoc);
        gl.vertexAttribPointer(this.m_VertexLoc, Utils.VEC2, gl.FLOAT, gl.FALSE, 0, 0);
        


        // 1 STEP
        // Rotation matrix:M symmetric on Y axis matrix: M2
        let M = mat4.create();
        let M2 = mat4.create();
        let M3 = mat4.create();
        mat4.identity(M3);
        mat4.identity(M2);
        mat4.identity(M);

        // // Get rotation matrix and symmetric transformation from vertex shaders
        let rota = gl.getUniformLocation(this.m_ShaderId, "rotationZmatrix");
        let symetric = gl.getUniformLocation(this.m_ShaderId, "symetricYmatrix");
        let scale = gl.getUniformLocation(this.m_ShaderId, "scaleMatrix");

        gl.uniformMatrix4fv(symetric, false, M2);

        gl.uniformMatrix4fv(scale, false, M3);
        gl.uniformMatrix4fv(rota, false, M);

        gl.drawArrays(gl.LINES, 0, this.vertices.length/2);

        // désactiver le buffer
        gl.disableVertexAttribArray(this.m_VertexLoc);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        // désactiver le shader
        gl.useProgram(null);
    }
    /** dessiner l'objet */
    onDraw_1()
    {   

        // activer le shader
        gl.useProgram(this.m_ShaderId);

        // activer et lier le buffer contenant les coordonnées
        gl.bindBuffer(gl.ARRAY_BUFFER, this.m_VertexBufferId);
        gl.enableVertexAttribArray(this.m_VertexLoc);
        gl.vertexAttribPointer(this.m_VertexLoc, Utils.VEC2, gl.FLOAT, gl.FALSE, 0, 0);
        


        // 1 STEP
        // Rotation matrix:M symmetric on Y axis matrix: M2
        let M = mat4.create();
        let M2 = mat4.create();
        let M3 = mat4.create();
        mat4.identity(M3);
        mat4.identity(M2);
        mat4.identity(M);

        // Get rotation matrix and symmetric transformation from vertex shaders
        let rota = gl.getUniformLocation(this.m_ShaderId, "rotationZmatrix");
        let symetric = gl.getUniformLocation(this.m_ShaderId, "symetricYmatrix");
        let scale = gl.getUniformLocation(this.m_ShaderId, "scaleMatrix");

        gl.uniformMatrix4fv(symetric, false, M2);
        gl.uniformMatrix4fv(scale, false, M3);


        for(let i = 0; i < 6; i++) {

            // Compute rotation
            mat4.rotateZ(M, M, -((2*Math.PI)/6));

            // Apply rotation to vertex shaders
            gl.uniformMatrix4fv(rota, false, M);

            // Draw it
            gl.drawArrays(gl.LINES, 0, this.vertices.length/2);
        }

        // désactiver le buffer
        gl.disableVertexAttribArray(this.m_VertexLoc);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        // désactiver le shader
        gl.useProgram(null);
    }
    onDraw_2()
    {   
    
        // activer le shader
        gl.useProgram(this.m_ShaderId);

        // activer et lier le buffer contenant les coordonnées
        gl.bindBuffer(gl.ARRAY_BUFFER, this.m_VertexBufferId);
        gl.enableVertexAttribArray(this.m_VertexLoc);
        gl.vertexAttribPointer(this.m_VertexLoc, Utils.VEC2, gl.FLOAT, gl.FALSE, 0, 0);
        


        // 1 STEP
        // Rotation matrix:M symmetric on Y axis matrix: M2
        let M = mat4.create();
        let M2 = mat4.create();
        let M3 = mat4.create();
        mat4.identity(M3);
        mat4.identity(M2);
        mat4.identity(M);

        // Get rotation matrix and symmetric transformation from vertex shaders
        let rota = gl.getUniformLocation(this.m_ShaderId, "rotationZmatrix");
        let symetric = gl.getUniformLocation(this.m_ShaderId, "symetricYmatrix");
        let scale = gl.getUniformLocation(this.m_ShaderId, "scaleMatrix");

        gl.uniformMatrix4fv(symetric, false, M2);
        gl.uniformMatrix4fv(scale, false, M3);
        gl.uniformMatrix4fv(rota, false, M);


        for(let i = 0; i < 6; i++) {

            // Compute rotation
            mat4.rotateZ(M, M, -((2*Math.PI)/6));

            // Apply rotation to vertex shaders
            gl.uniformMatrix4fv(rota, false, M);

            // Draw it
            gl.drawArrays(gl.LINES, 0, this.vertices.length/2);
        }


        // 2ND STEP
        // Compute Symmetric on Y axis (= identity matrix except that: y -> -y):
        M2[5] = -1;     
        gl.uniformMatrix4fv(symetric, false, M2);

        gl.drawArrays(gl.LINES, 0, this.vertices.length/2);

        // désactiver le buffer
        gl.disableVertexAttribArray(this.m_VertexLoc);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        // désactiver le shader
        gl.useProgram(null);
    }
    onDraw_3()
    {   
    
        // activer le shader
        gl.useProgram(this.m_ShaderId);

        // activer et lier le buffer contenant les coordonnées
        gl.bindBuffer(gl.ARRAY_BUFFER, this.m_VertexBufferId);
        gl.enableVertexAttribArray(this.m_VertexLoc);
        gl.vertexAttribPointer(this.m_VertexLoc, Utils.VEC2, gl.FLOAT, gl.FALSE, 0, 0);
        


        // 1 STEP
        // Rotation matrix:M symmetric on Y axis matrix: M2
        let M = mat4.create();
        let M2 = mat4.create();
        let M3 = mat4.create();
        mat4.identity(M3);
        mat4.identity(M2);
        mat4.identity(M);

        // Get rotation matrix and symmetric transformation from vertex shaders
        let rota = gl.getUniformLocation(this.m_ShaderId, "rotationZmatrix");
        let symetric = gl.getUniformLocation(this.m_ShaderId, "symetricYmatrix");
        let scale = gl.getUniformLocation(this.m_ShaderId, "scaleMatrix");

        gl.uniformMatrix4fv(symetric, false, M2);
        gl.uniformMatrix4fv(scale, false, M3);


        for(let i = 0; i < 6; i++) {

            // Compute rotation
            mat4.rotateZ(M, M, -((2*Math.PI)/6));

            // Apply rotation to vertex shaders
            gl.uniformMatrix4fv(rota, false, M);

            // Draw it
            gl.drawArrays(gl.LINES, 0, this.vertices.length/2);
        }


        // 2ND STEP
        // Compute Symmetric on Y axis (= identity matrix except that: y -> -y):
        M2[5] = -1;     
        gl.uniformMatrix4fv(symetric, false, M2);

        // 3RD STEP
        for(let k = 0; k < 6; k ++) {
            // Compute rotation on symmetric
            mat4.rotateZ(M, M, -((2*Math.PI)/6));

            // Apply rotation to vertex shaders
            gl.uniformMatrix4fv(rota, false, M);

            // Draw it
            gl.drawArrays(gl.LINES, 0, this.vertices.length/2);
        }
 



        // désactiver le buffer
        gl.disableVertexAttribArray(this.m_VertexLoc);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        // désactiver le shader
        gl.useProgram(null);
    }
    onDraw_4()
    {   
    
        // activer le shader
        gl.useProgram(this.m_ShaderId);

        // activer et lier le buffer contenant les coordonnées
        gl.bindBuffer(gl.ARRAY_BUFFER, this.m_VertexBufferId);
        gl.enableVertexAttribArray(this.m_VertexLoc);
        gl.vertexAttribPointer(this.m_VertexLoc, Utils.VEC2, gl.FLOAT, gl.FALSE, 0, 0);
        


        // 1 STEP
        // Rotation matrix:M symmetric on Y axis matrix: M2
        let M = mat4.create();
        let M2 = mat4.create();
        let M3 = mat4.create();
        mat4.identity(M3);
        mat4.identity(M2);
        mat4.identity(M);


        // Get rotation matrix and symmetric transformation from vertex shaders
        let rota = gl.getUniformLocation(this.m_ShaderId, "rotationZmatrix");
        let symetric = gl.getUniformLocation(this.m_ShaderId, "symetricYmatrix");
        let scale = gl.getUniformLocation(this.m_ShaderId, "scaleMatrix");

        gl.uniformMatrix4fv(symetric, false, M2);
        // M3 = mat4.multiplyScalar(M3, 3.);
        // M3[0] = 2;
        M3[4] = 0.5;
        M3[8] = 0.6;
        // console.log(mat4.scale(M3, 2));
        gl.uniformMatrix4fv(scale, false, M3);


        for(let i = 0; i < 6; i++) {

            // Compute rotation
            mat4.rotateZ(M, M, -((2*Math.PI)/6));

            // Apply rotation to vertex shaders
            gl.uniformMatrix4fv(rota, false, M);

            // Draw it
            gl.drawArrays(gl.LINES, 0, this.vertices.length/2);
        }


        // 2ND STEP
        // Compute Symmetric on Y axis (= identity matrix except that: y -> -y):
        M2[5] = -1;     
        gl.uniformMatrix4fv(symetric, false, M2);

        // 3RD STEP
        for(let k = 0; k < 6; k ++) {
            // Compute rotation on symmetric
            mat4.rotateZ(M, M, -((2*Math.PI)/6));

            // Apply rotation to vertex shaders
            gl.uniformMatrix4fv(rota, false, M);

            // Draw it
            gl.drawArrays(gl.LINES, 0, this.vertices.length/2);
        }
 



        // désactiver le buffer
        gl.disableVertexAttribArray(this.m_VertexLoc);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        // désactiver le shader
        gl.useProgram(null);
    }

    onDraw_5()
    {   
    
        // activer le shader
        gl.useProgram(this.m_ShaderId);

        // activer et lier le buffer contenant les coordonnées
        gl.bindBuffer(gl.ARRAY_BUFFER, this.m_VertexBufferId);
        gl.enableVertexAttribArray(this.m_VertexLoc);
        gl.vertexAttribPointer(this.m_VertexLoc, Utils.VEC2, gl.FLOAT, gl.FALSE, 0, 0);
        


        // 1 STEP
        // Rotation matrix:M symmetric on Y axis matrix: M2
        let M = mat4.create();
        let M2 = mat4.create();
        let M3 = mat4.create();
        
        mat4.identity(M2);
        mat4.identity(M);
        // M3 is a scaling matrix
        let scaleVec = vec3.fromValues(0.5, 0.5, 0.5);
        console.log(scaleVec);
        mat4.fromScaling(M3, scaleVec);


        // Get rotation matrix and symmetric transformation from vertex shaders
        let rota = gl.getUniformLocation(this.m_ShaderId, "rotationZmatrix");
        let symetric = gl.getUniformLocation(this.m_ShaderId, "symetricYmatrix");
        let scale = gl.getUniformLocation(this.m_ShaderId, "scaleMatrix");

        gl.uniformMatrix4fv(symetric, false, M2);
        gl.uniformMatrix4fv(scale, false, M3);


        for(let i = 0; i < 6; i++) {

            // Compute rotation
            mat4.rotateZ(M, M, -((2*Math.PI)/6));

            // Apply rotation to vertex shaders
            gl.uniformMatrix4fv(rota, false, M);

            // Draw it
            gl.drawArrays(gl.LINES, 0, this.vertices.length/2);
        }


        // 2ND STEP
        // Compute Symmetric on Y axis (= identity matrix except that: y -> -y):
        M2[5] = -1;     
        gl.uniformMatrix4fv(symetric, false, M2);

        // 3RD STEP
        for(let k = 0; k < 6; k ++) {
            // Compute rotation on symmetric
            mat4.rotateZ(M, M, -((2*Math.PI)/6));

            // Apply rotation to vertex shaders
            gl.uniformMatrix4fv(rota, false, M);

            // Draw it
            gl.drawArrays(gl.LINES, 0, this.vertices.length/2);
        }
 



        // désactiver le buffer
        gl.disableVertexAttribArray(this.m_VertexLoc);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        // désactiver le shader
        gl.useProgram(null);
    }





    /** destructeur */
    destroy()
    {
        // supprimer le shader et le VBO
        Utils.deleteShaderProgram(this.m_ShaderId);
        Utils.deleteVBO(this.m_VertexBufferId);
    }
}
