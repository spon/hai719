function randColor() {
    return [Math.random(), Math.random(), Math.random(), 1];
}

// doc de la fonction de base : https://easings.net/#easeOutBounce
// ajout du squash-and-stretch sur le rebond (qui scale en fonction du déplacement de la balle)
function bounce(x, scaling) {
    const n1 = 7.5625;
    const d1 = 2.75;
    let y;
    if (x < 1 / d1) {
        y = n1 * x * x;
        scaling = [1.3*y, 1/y, 1];
        return {
            y : y,
            scaling : scaling
        }

    } else if (x < 2 / d1) {
        y = n1 * (x -= 1.5 / d1) * x + 0.75;
        scaling = [1.3*y, 1/y, 1];
        return {
            y : y,
            scaling : scaling
        }

    } else if (x < 2.5 / d1) {
        y = n1 * (x -= 2.25 / d1) * x + 0.9375;
        scaling = [1.3*y, 1/y, 1];
        return {
            y : y,
            scaling : scaling
        } 

    } else {
        y = n1 * (x -= 2.625 / d1) * x + 0.984375;
        scaling = [1.3*y, 1/y, 1];
        return {
            y : y,
            scaling : scaling
        }
    }
}

// interpolation linéaire avec la fonction de rebond
function lerp(src, dest, T, t) {
    let v = new Array();
    let res = new Array();
    let scale = new Array();
    src.forEach((elem, index) => {
        v[index] = dest[index] - elem;
    });
    

    // bounce animation
    let b = bounce(t, scale);
    let y = b.y;
    res[0] = v[0] * 0.5 * t;          // mouvement horizontal
    res[1] = v[1] * y;          //mouvement vertical
    scale = b.scaling

    return {
        pos : res,
        scale : scale
    }
}

//interpolation linéaire avec le "roulement"
function roll(src, dest, t) {

    let v = new Array();
    let res = new Array();
    let scale = new Array();
    src.forEach((elem, index) => {
        v[index] = dest[index] - elem;
    });

    //linear animation
    src.forEach((elem, index) => {
        res[index] =  t * v[index];
    });
    scale = [1, 1., 1.];

    return {
        pos : res,
        scale : scale
    }
    
}


function disk(gl, radius, settings, res, color_used) {
    let center = settings.src;
    let vertices_comp = new Array(res);
    vertices_comp.fill(0);
    let angle = (2 * Math.PI) / res;
    let x, y;

    // cercle de résolution res
    const position_buffer = gl.createBuffer();
    let vertices = vertices_comp.map(function(el, index) {
        x = center[0] + radius * Math.cos(index * angle);    
        y = center[1] + radius * Math.sin(index * angle);
        return [x, y];
    });

    // sommets du disque
    let vertices_array = vertices.flat();

    // const color_used = randColor();
    
    gl.bindBuffer(gl.ARRAY_BUFFER, position_buffer);
    // passage des positions à webGL
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices_array), gl.STATIC_DRAW);
    
    
    return {
        position : position_buffer,
        color : color_used,
        nb_vertices : res,
        dimension : 2,
        settings : settings
    }

}


main();

function main() {
    const canvas = document.querySelector("#glCanvas");
    const bold_txt = document.querySelector("#ttl");

    let M1 = mat4.create();
    mat4.identity(M1);
    bold_txt.textContent = `TP5: animation`;

    const gl = canvas.getContext("webgl");
    // const edges = document.querySelector("#edges");


    if(!gl){
        console.log("impossible d'initialiser webGL!!");
        return;
    }

    // Définir la couleur d'effacement comme étant le noir, complètement opaque
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    // Effacer le tampon de couleur avec la couleur d'effacement spécifiée
    gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);


    //shaders

    const vertex_shaders = dedent
        `
        attribute vec4 vertex_position;

        uniform mat4 model_view_matrix;
        uniform mat4 projection_matrix;
        uniform mat4 scaling_matrix;


        void main() {
            gl_Position =  scaling_matrix * projection_matrix * model_view_matrix * vertex_position;
        }
        `;

    const fragment_shaders = dedent
        `
        uniform highp vec4 col;
        void main() {
            gl_FragColor = col;
        }
        `;

    
    
    // Renvoie le shader crée et compilé
    function load_shader(gl, type, source) {
        const shader = gl.createShader(type);

        //code source envoyé à l'objet shader
        gl.shaderSource(shader, source);

        gl.compileShader(shader);

        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            console.log("erreur à la compilation des shaders."+ gl.getShaderInfoLog(shader));
            gl.deleteShader(shader);
            return null;
        }

        return shader;
    }



    // Initialise le programme shader en liant les vertex_shader et fragment_shader
    function init_shader_program(gl, v_shader_src, f_shader_src) {
        const vertex_shader = load_shader(gl, gl.VERTEX_SHADER, v_shader_src);
        const fragment_shader = load_shader(gl, gl.FRAGMENT_SHADER, f_shader_src);


        // Création du programme shader avec le vertex_shader et fragment_shader
        const shader_program = gl.createProgram();
        gl.attachShader(shader_program, vertex_shader);
        gl.attachShader(shader_program, fragment_shader);
        gl.linkProgram(shader_program);

        if (!gl.getProgramParameter(shader_program, gl.LINK_STATUS)) {
            console.log("impossible d'initialiser le programme shader!!");
            return null;
        }
        
        return shader_program;
    }

  
   
    function draw_scene(gl, program_info, buffers, animation) {
        gl.clearColor(0.0, 0.0, 0.0, 1.0);  // effacement en noir, complètement opaque
        gl.clearDepth(1.0);                 // tout effacer
        gl.enable(gl.DEPTH_TEST);           // activer le test de profondeur
        gl.depthFunc(gl.LEQUAL);            // les choses proches cachent les choses lointaines

        // effacer le canvas avant de dessiner dessus
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


        // Créer une matrice de perspective, une matrice spéciale qui est utilisée pour
        // simuler la distorsion de la perspective dans une caméra.
        // Notre champ de vision est de 90 degrés, avec un rapport largeur/hauteur qui
        // correspond à la taille d'affichage du canvas ;
        // et nous voulons seulement voir les objets situés entre 0,1 unité et 100 unités
        // à partir de la caméra.

        const fieldOfView = 90 * Math.PI / 180;   // en radians
        const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
        const zNear = 0.1;
        const zFar = 100.0;
        const projection_matrix = mat4.create();
        
        // projection_matrix -> camera
        mat4.perspective(
            projection_matrix,
            fieldOfView,
            aspect,
            zNear,
            zFar
            );
        
        let model_view_matrix = mat4.create();
        
        //interpolation linéaire entre src et dest pour une animation de durée duration à un temps time
        // let lerp_info = lerp(buffers.settings.src, buffers.settings.dest, buffers.settings.duration, time);
        let pos = animation.pos;
        let scale_vec = animation.scale;
        pos[2] = -5.;
        
        let scaling_matrix = mat4.create();
        mat4.scale(scaling_matrix, scaling_matrix, scale_vec);
        // console.log(`nextpos: ${pos}`);

        mat4.translate(model_view_matrix, model_view_matrix, pos);
        //reculer l'objet
        // mat4.translate(model_view_matrix, model_view_matrix, [0., 0., -5.]);
        
        
        gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
        // extraire 2 (point en 2D) valeurs par itération, pas d'offset, pas de stride, pas de normalisation (false)
        gl.enableVertexAttribArray(program_info.attrib_location.vertex_position);
        gl.vertexAttribPointer(
            program_info.attrib_location.vertex_position,
            buffers.dimension,
            gl.FLOAT,
            false,
            0,
            0
        );


        gl.useProgram(program_info.program);


        gl.uniformMatrix4fv(
            program_info.uniform_location.projection_matrix,
            false,
            projection_matrix
        );

        gl.uniformMatrix4fv(
            program_info.uniform_location.scaling_matrix,
            false,
            scaling_matrix
        );

   
        gl.uniformMatrix4fv(
            program_info.uniform_location.model_view_matrix,
            false,
            model_view_matrix
        );
        
        // couleur unique pour le disque (uniform)
        const color = buffers.color;
        gl.uniform4f(
            program_info.uniform_location.v_col,
            false,
            color[0],
            color[1],
            color[2],
            color[3]
        );

        console.log(buffers.nb_vertices);
        gl.drawArrays(gl.TRIANGLE_FAN, 0, buffers.nb_vertices);

    }

    const shader_prog = init_shader_program(gl, vertex_shaders, fragment_shaders);
    // Objet utile pour modifier les valeurs des attributs et uniforms 
    // dans les fragments.
    const program_info = {
        program : shader_prog,

        attrib_location : {
            vertex_position : gl.getAttribLocation(shader_prog, 'vertex_position'),
        },

        uniform_location : {
            projection_matrix : gl.getUniformLocation(shader_prog, 'projection_matrix'),
            model_view_matrix : gl.getUniformLocation(shader_prog, 'model_view_matrix'),
            scaling_matrix : gl.getUniformLocation(shader_prog, 'scaling_matrix'),
            v_col: gl.getUniformLocation(shader_prog, 'col')
        }
    };
    
    

    // position initiale de la balle et taille
    let initial_pos = [0, 4.4, 0.]; // goal -> [0, -3.5] (bas du canvas)
    let goal_pos = [3, -4.4, 0.];
    let T = 4;                     // animation duration

    let disk_settings = {
        src : initial_pos,
        dest : goal_pos,
        current_pos : initial_pos,
        duration : T
    };

    let sz = 0.6;
    let current_res = 30;
    let col = randColor();
    
    let then = 0;
    function Animation() {
        requestAnimationFrame(function fct(time) {
            time *= 0.001;
            let deltaTime = time  - then;       // temps passé entre 2 frames
            then = time;

            let frameRate = 1/deltaTime;
            const relative_t = time / disk_settings.duration;
            console.log(`framerate: ${frameRate}fps, dt=${relative_t}, delta=${deltaTime}`);
            
            let d = disk(gl, sz, disk_settings, current_res, col);
            let lerp_info;
            let id;
            if(relative_t <= 1){
                
                id = requestAnimationFrame(fct);
                lerp_info = lerp(d.settings.src, d.settings.dest, d.settings.duration, relative_t);
             
                draw_scene(gl, program_info, d, lerp_info);

            }else if(relative_t <= 2) {
                let new_src = [3.9, -4.4, 0];
                let new_dest = [6, -4.4, 0];
                disk_settings = {
                    src : new_src,
                    dest : new_dest,
                    current_pos : initial_pos,
                    duration : T
                };
                console.log(d.settings);
                lerp_info = roll(new_src, new_dest, relative_t-2);
                draw_scene(gl, program_info, d, lerp_info);
                id = requestAnimationFrame(fct);

            }else if(relative_t <= 3) {

                cancelAnimationFrame(id);

            }


        });
    }
    
    Animation();

}