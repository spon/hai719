## Exerice 2

1. Pour le **squash-and-stretch** les coordonées dela balle ne seront pas modifiées on va utiliser différentes opérations de scale et de rotations.

4. Pour la fonction de rebond j'ai utilisé la base de code [easeOutBounce](https://easings.net/fr#easeOutBounce), en ajoutant des mises à l'échelle sur les différents axes pour le **squash-and-stretch**: lorsque la balle bouge verticalement elle est *étirée* verticalement et lorsqu'elle bouge moins, elle est étirée horizontalement.

___
Par manque de temps je n'ai pas pu finir d'implémenter le "rembobinage" ni la boucle infini.