#version 450 core // Minimal GL version support expected from the GPU

layout(location=0) in vec3 vPosition; // The 1st input attribute is the position (CPU side: glVertexAttrib 0)
layout(location=1) in vec3 vNormal;
layout(location=2) in vec2 vTexCoord;

uniform mat4 projectionMat, viewMat, modelMat, normalMat; 

out vec3 fPosition;
out vec4 fShadowPos;
out vec3 fNormal;
out vec2 fTexCoord;

uniform mat4 depthMVP; //matrice de transformation pour passer au point de vue de la light.

void main() {
    vec4 n = normalMat * vec4 (vNormal, 1.0);

    fPosition = vec3(viewMat * modelMat * vec4(vPosition, 1.0));
    fNormal = normalize (n.xyz);
    fTexCoord = vTexCoord;
    fShadowPos = depthMVP * modelMat * vec4(vPosition, 1.0);
    gl_Position =  projectionMat * viewMat * modelMat * vec4(vPosition, 1.0); // mandatory to fire rasterization properly
    
}
