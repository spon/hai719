#version 450 core // Minimal GL version support expected from the GPU

struct LightSource {
    vec3 position;
    vec3 color;
    float intensity;
    int isActive;
};
int number_of_lights = 3;

uniform sampler2D shadowMap;        // texture contenant la shadowmap

uniform LightSource lightSources[3];

struct Material {
    vec3 albedo;
    float shininess;
};

uniform Material material;

in vec3 fPosition; // Shader input, linearly interpolated by default from the previous stage (here the vertex shader)
in vec4 fShadowPos; // position dans le point de vue de la lumière.
in vec3 fNormal;
in vec2 fTexCoord;

out vec4 colorResponse; // Shader output: the color response attached to this fragment


uniform mat4 projectionMat, viewMat, modelMat, normalMat;


float pi = 3.1415927;

// renvoie true si le fragment est dans l'ombre.
bool shadowCompute(vec4 fragPosLight, float bias) {

    vec3 projCoords = fragPosLight.xyz / fragPosLight.w;    // utile si on utilise une projection en perspective
    projCoords = projCoords * 0.5 + 0.5;
    float closestDepth = texture(shadowMap, projCoords.xy).r;
    float currentDepth = projCoords.z;
    return currentDepth - bias > closestDepth? true : false;
}

void main() {
    vec3 n = normalize(fNormal);

    // Linear barycentric interpolation does not preserve unit vectors
    vec3 wo = normalize (-fPosition); // unit vector pointing to the camera
    vec3 radiance = vec3(0,0,0);
    mat4 modelViewMat = viewMat * modelMat;

    bool shadow = false;
    if( dot( n , wo ) >= 0.0 ) {
        {
            for(int i = 0; i < number_of_lights; i ++){

                if( lightSources[i].isActive == 1 ) { // WE ONLY CONSIDER LIGHTS THAT ARE SWITCHED ON
                    
                    {
                        shadow = shadowCompute(fShadowPos, 0.005);
                        vec3 wi = normalize ( vec3((modelViewMat * vec4(lightSources[i].position,1)).xyz) - fPosition ); // unit vector pointing to the light source (change if you use several light sources!!!)
                        if( dot( wi , n ) >= 0.0 ) { // WE ONLY CONSIDER LIGHTS THAT ARE ON THE RIGHT HEMISPHERE (side of the tangent plane)
                            vec3 wh = normalize( wi + wo ); // half vector (if wi changes, wo should change as well)
                            vec3 Li = lightSources[i].color * lightSources[i].intensity;

                            radiance = radiance +
                                    Li // light color
                                    * material.albedo
                                    * ( max(dot(n,wi),0.0) + pow(max(dot(n,wh),0.0),material.shininess) )
                                    ;
                        }
                    }
                    
                }
            }
        }
    }

    if(shadow)
        radiance *= 0.3;

    colorResponse = vec4 (radiance, 1.0); // Building an RGBA value from an RGB one.
}




