## Shadowmaps

### Partie 1

La construction de la shadowmap semble être bonne:

| ![rendu](./screens/rendu.png) | <img src="./screens/sha.png" alt="shadowmap2" style="zoom:20%;" /> |
| ----------------------------- | ------------------------------------------------------------ |

 

### Partie 2
1. Il faut bien faire attention à fournir l'identifiant (int) de la texture pour la lumière courante au shader et non pas la texture elle-même:

```c++
shaderProgramPtr->set("depthMVP", (glm::mat4) light.depthMVP);
shaderProgramPtr->set("shadowMap", (int) light.shadowMapTexOnGPU);
```

2. Au niveau du rendu des ombres j'avais un clip space trop petit et donc l'ombre n'était pas correctement rendue, il m'a fallu l'augmenter (*2):

   | ![bad_clipping](./screens/bad_clipSpace.png) | ![good_clipping](./screens/right_clip_space.png) |
   | -------------------------------------------- | ------------------------------------------------ |

   

3. Pour éviter les artefacts d'auto-ombrage, j'ai simplement ajouté un biais dans le calcul de l'ombrage du fragment:

   ```c++
    return currentDepth - bias > closestDepth? true : false;
   ```

   ### Partie 3

   De base 3 lumières de couleurs, d'intensités et de positions différentes sont ajoutées à la scène cependant la texture d'ombres affichée est celle de la **dernière lumière ajoutée**. L'utilisateur peut modifier la texture d'ombre à afficher en appuyant sur la touche **L**. Cette fonctionnalité n'est pas vraiment au point puisque les autres textures d'ombres ne sont pas complètement bien rendues. Pour y remédier il faudrait pouvoir comparer la position du fragment avec toutes les textures pour rendre les ombres correspondant à toutes les lumières mais je n'y suis pas parvenu. 
