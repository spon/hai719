/**
  * Create a model of a cube, centered at the origin.  (This is not
  * a particularly good format for a cube, since an IFS representation
  * has a lot of redundancy.)
  * @side the length of a side of the cube.  If not given, the value will be 1.
  */
 function tetra(side) {
    var s = (side || 1)/2;
    var coords = [];
    var normals = [];
    var texCoords = [];
    var indices = [];
    function face(xyz, nrm) {
       var start = coords.length/3;
       var i;
       for (i = 0; i < 9; i++) {
          coords.push(xyz[i]);
       }
       for (i = 0; i < 4; i++) {
          normals.push(nrm[0],nrm[1],nrm[2]);
       }
       texCoords.push(1,0,1,1,0,1);                         // coordonées s,t de la texture sur chaque face
    //    indices.push(start,start+1,start+2,start,start+2,start+3);
       indices.push(start,start+1,start+2);
    }
    face( [s,-s,s, s,s,s, -s, s, s], [0,0,1] ); //front
    face( [-s,s,s, s,s,s, s,s,-s], [0,1,0] ); //top
    face( [s,-s,s, s,s,s, s,s,-s], [0,1,0] ); //right
    face( [ s,s,-s, -s,s,s, s,-s,s], [1,0,0] );  //diag
    
    return {
       vertexPositions: new Float32Array(coords),
       vertexNormals: new Float32Array(normals),
       vertexTextureCoords: new Float32Array(texCoords),
       texCoords : new Float32Array(texCoords),
       indices: new Uint16Array(indices)
    };
 }


main();

function main() {
    const canvas = document.querySelector("#glCanvas");
    const bold_txt = document.querySelector("#ttl");

    let M1 = mat4.create();
    mat4.identity(M1);
    bold_txt.textContent = `TP4: textures`;

    const gl = canvas.getContext("webgl");
    // const edges = document.querySelector("#edges");
    const size = document.querySelector("#size");
    const rotate_y = document.querySelector("#rotate_y");
    const rotate_x = document.querySelector("#rotate_x");

    if(!gl){
        console.log("impossible d'initialiser webGL!!");
        return;
    }

    // Définir la couleur d'effacement comme étant le noir, complètement opaque
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    // Effacer le tampon de couleur avec la couleur d'effacement spécifiée
    gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);


    //shaders

    const vertex_shaders = dedent
        `
        attribute vec4 vertex_position;
        attribute vec2 texture_coord;

        uniform mat4 model_view_matrix;
        uniform mat4 projection_matrix;

        varying highp vec2 v_texture_coord;

        void main() {
            gl_Position = projection_matrix * model_view_matrix * vertex_position;
            v_texture_coord = texture_coord;
        }
        `;

    const fragment_shaders = dedent
        `
        varying highp vec2 v_texture_coord;

        uniform sampler2D sampler;
        void main() {
            gl_FragColor = texture2D(sampler, v_texture_coord);
        }
        `;

    
    
    // Renvoie le shader crée et compilé
    function load_shader(gl, type, source) {
        const shader = gl.createShader(type);

        //code source envoyé à l'objet shader
        gl.shaderSource(shader, source);

        gl.compileShader(shader);

        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            console.log("erreur à la compilation des shaders."+ gl.getShaderInfoLog(shader));
            gl.deleteShader(shader);
            return null;
        }

        return shader;
    }

    // initialisation de la texture
    function load_texture(gl, url) {
        const texture = gl.createTexture();
        
        const image = new Image();
        image.src = url;

        const level = 0;
        const internalFormat = gl.RGBA;
        const width = 1;
        const height = 1;
        const border = 0;
        const srcFormat = gl.RGBA;
        const srcType = gl.UNSIGNED_BYTE;
        const pixel = new Uint8Array([0, 0, 255, 255]);
       
        
        image.addEventListener("load", function() {
            gl.bindTexture(gl.TEXTURE_2D, texture);
            gl.texImage2D(gl.TEXTURE_2D, level, internalFormat, srcFormat, srcType, image);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            
        });

         //With null as the last parameter, the previous method allocates memory for the texture and fills it with zeros.
         // The previous line sets the minification filter to gl.LINEAR so we won't neet mipmaps
        return texture;

    }

    // Initialise le programme shader en liant les vertex_shader et fragment_shader
    function init_shader_program(gl, v_shader_src, f_shader_src) {
        const vertex_shader = load_shader(gl, gl.VERTEX_SHADER, v_shader_src);
        const fragment_shader = load_shader(gl, gl.FRAGMENT_SHADER, f_shader_src);


        // Création du programme shader avec le vertex_shader et fragment_shader
        const shader_program = gl.createProgram();
        gl.attachShader(shader_program, vertex_shader);
        gl.attachShader(shader_program, fragment_shader);
        gl.linkProgram(shader_program);

        if (!gl.getProgramParameter(shader_program, gl.LINK_STATUS)) {
            console.log("impossible d'initialiser le programme shader!!");
            return null;
        }
        
        return shader_program;
    }

    //première fonction pour initialiser un buffer webGL avec des positions
    function init_buffers_2D(gl, size, nb_vert, rot) {
        const position_buffer = gl.createBuffer();
        const color_buffer = gl.createBuffer();

        // angle de rotation pour passer d'un point à l'autre (en rad)
        const angle = (360 / nb_vert) * (Math.PI/180);

        let positions = Array(nb_vert);
        positions.fill([0., 0.]);

        let current_pt = [size, size];
        positions = positions.map(function(pos) {
            // Matrice de rotation appliquée sur le point courant pour obtenir le prochain point
            let x = Math.fround((Math.cos(angle) * current_pt[0]) - (Math.sin(angle) * current_pt[1]));
            let y = Math.fround((Math.sin(angle) * current_pt[0]) + (Math.cos(angle) * current_pt[1]));
            current_pt = [x,y];
            
            return [x,y];
        }).flat();

        const colors = [
            1.0,  1.0,  1.0,  1.0,    // blanc
            1.0,  0.0,  0.0,  1.0,    // rouge
            0.0,  1.0,  0.0,  1.0,    // vert
            0.0,  0.0,  1.0,  1.0,    // bleu
        ];

        gl.bindBuffer(gl.ARRAY_BUFFER, position_buffer);
        // passage des positions à webGL
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);
        
        //couleurs
        gl.bindBuffer(gl.ARRAY_BUFFER, color_buffer);
        // passage des couleurs à webGL
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

        return {
            position : position_buffer,
            color : color_buffer,
            nb_vertices : nb_vert,
            rotation : rot,
            dimension : 2
        };
    }

    // création du buffer pour le tétraèdre (en travaux ...)
    function init_buffers_tetra(gl, size, rot) {
        const position_buffer = gl.createBuffer();
        const color_buffer = gl.createBuffer();
        const index_buffer = gl.createBuffer();
        const texture_coord_buffer = gl.createBuffer();

        let tetrahedron = tetra(size);

        const colors = [
            1.0,  1.0,  1.0,  1.0,    // blanc
            1.0,  0.0,  0.0,  1.0,    // rouge
            0.0,  1.0,  0.0,  1.0,    // vert
            0.0,  0.0,  1.0,  1.0,    // bleu
        ];


        gl.bindBuffer(gl.ARRAY_BUFFER, position_buffer);
        // passage des positions à webGL
        gl.bufferData(gl.ARRAY_BUFFER, tetrahedron.vertexPositions, gl.STATIC_DRAW);

        //couleurs
        gl.bindBuffer(gl.ARRAY_BUFFER, color_buffer);
        // passage des couleurs à webGL
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
        
        //indices
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_buffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, tetrahedron.indices, gl.STATIC_DRAW);

        //texture coordinates
        gl.bindBuffer(gl.ARRAY_BUFFER, texture_coord_buffer);
        gl.bufferData(gl.ARRAY_BUFFER, tetrahedron.texCoords, gl.STATIC_DRAW);

        return {
            position : position_buffer,
            color : color_buffer,
            indices : index_buffer,
            texture_coord : texture_coord_buffer,
            nb_vertices : 8,

            rotation : {
                x : rot.x,
                y : rot.y
            },

            dimension: 3,
            count : tetrahedron.indices.length
        };

    }

    function draw_scene(gl, program_info, buffers, texture) {
        gl.clearColor(0.0, 0.0, 0.0, 1.0);  // effacement en noir, complètement opaque
        gl.clearDepth(1.0);                 // tout effacer
        gl.enable(gl.DEPTH_TEST);           // activer le test de profondeur
        gl.depthFunc(gl.LEQUAL);            // les choses proches cachent les choses lointaines

        // effacer le canvas avant de dessiner dessus
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

        // Créer une matrice de perspective, une matrice spéciale qui est utilisée pour
        // simuler la distorsion de la perspective dans une caméra.
        // Notre champ de vision est de 45 degrés, avec un rapport largeur/hauteur qui
        // correspond à la taille d'affichage du canvas ;
        // et nous voulons seulement voir les objets situés entre 0,1 unité et 100 unités
        // à partir de la caméra.

        const fieldOfView = 45 * Math.PI / 180;   // en radians
        const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
        const zNear = 0.1;
        const zFar = 100.0;
        const projection_matrix = mat4.create();
        
        // projection_matrix -> camera
        mat4.perspective(
            projection_matrix,
            fieldOfView,
            aspect,
            zNear,
            zFar
            );
        
        const model_view_matrix = mat4.create();
        
        //reculer l'objet
        mat4.translate(model_view_matrix, model_view_matrix, [0., 0., -10.]);

        
        gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position);
        // extraire 2 (point en 2D) valeurs par itération, pas d'offset, pas de stride, pas de normalisation (false)
        gl.vertexAttribPointer(
            program_info.attrib_location.vertex_position,
            buffers.dimension,
            gl.FLOAT,
            false,
            0,
            0
        );

        gl.enableVertexAttribArray(program_info.attrib_location.vertex_position);
        
        //texture
        gl.bindBuffer(gl.ARRAY_BUFFER, buffers.texture_coord);
        gl.vertexAttribPointer(
            program_info.attrib_location.texture_position,
            2,
            gl.FLOAT,
            false,
            0,
            0
        );
        
        gl.enableVertexAttribArray(program_info.attrib_location.texture_position);

        

        gl.useProgram(program_info.program);

        // Y rotation
        mat4.rotateY(model_view_matrix, model_view_matrix, buffers.rotation.y);
        gl.uniformMatrix4fv(
            program_info.uniform_location.projection_matrix,
            false,
            projection_matrix
        );

        // X rotation
        mat4.rotateX(model_view_matrix, model_view_matrix, buffers.rotation.x);
        gl.uniformMatrix4fv(
            program_info.uniform_location.projection_matrix,
            false,
            projection_matrix
        );

        gl.uniformMatrix4fv(
            program_info.uniform_location.model_view_matrix,
            false,
            model_view_matrix
        );
        
        // usage de l'unité de texture 0
        gl.activeTexture(gl.TEXTURE0);
        // link la texture à l'unité 0
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.uniform1i(program_info.uniform_location.sampler, 0);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);
        
        // gl.drawArrays(gl.TRIANGLE_FAN, 0, buffers.nb_vertices);
        gl.drawElements(gl.TRIANGLES, buffers.count, gl.UNSIGNED_SHORT, 0); // drawElements pour objets en 3D

    }

    const shader_prog = init_shader_program(gl, vertex_shaders, fragment_shaders);
    // const shader_prog = Utils.makeShaderProgram(vertex_shaders, fragment_shaders, "scr");
    // Objet utile pour modifier les valeurs des attributs et uniforms 
    // dans les fragments.
    const program_info = {
        program : shader_prog,

        attrib_location : {
            vertex_position : gl.getAttribLocation(shader_prog, 'vertex_position'),
            texture_position : gl.getAttribLocation(shader_prog, 'texture_coord')
        },

        uniform_location : {
            projection_matrix : gl.getUniformLocation(shader_prog, 'projection_matrix'),
            model_view_matrix : gl.getUniformLocation(shader_prog, 'model_view_matrix'),
            sampler : gl.getUniformLocation(shader_prog, 'sampler')
        }
    };
    
    let current_size = parseFloat(size.value);
    // let current_nb_edges = parseFloat(edges.value);
    let current_angle_y = parseFloat(rotate_y.value);
    let current_angle_x = parseFloat(rotate_y.value);

    // stock l'angle en X et en Y
    let rota = {
        x : current_angle_x,
        y : current_angle_y
    };


    const texture = load_texture(gl, 'tex.jpg');

    console.log(`params: taille:${current_size}, rota X, Y: ${rota.x}, ${rota.y} `);
    // draw_scene(gl, program_info, init_buffers_2D(gl, current_size, current_nb_edges, current_angle));
    draw_scene(gl, program_info, init_buffers_tetra(gl, current_size, rota), texture);
    // edges.addEventListener("input", function(event) {
    //     draw_scene(gl, program_info, init_buffers_2D(gl, current_size, parseFloat(event.target.value), current_angle), texture);
    //     current_nb_edges = parseFloat(event.target.value);
    // });
    
    size.addEventListener("input", function(event) {
        current_size = parseFloat(event.target.value);
        draw_scene(gl, program_info, init_buffers_tetra(gl, current_size, rota), texture);
    });

    rotate_y.addEventListener("input", function(event) {
        let rota_rad = parseFloat(event.target.value) * (Math.PI / 180);
        current_angle_y = rota_rad;
        rota.y = rota_rad;
        draw_scene(gl, program_info, init_buffers_tetra(gl, current_size, rota), texture);
    });

    rotate_x.addEventListener("input", function(event) {
        let rota_rad = parseFloat(event.target.value) * (Math.PI / 180);
        current_angle_y = rota_rad;
        rota.x = rota_rad;
        draw_scene(gl, program_info, init_buffers_tetra(gl, current_size, rota), texture);
    });

 

}